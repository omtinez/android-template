[![Build Status](https://dev.azure.com/owahltinez/GitHubCI/_apis/build/status/omtinez.android-template?branchName=master)](https://dev.azure.com/owahltinez/GitHubCI/_build/latest?definitionId=1&branchName=master)

# Android Template
Minimal template for Android applications

## Features
- Written in Kotlin
- Implements a single activity pattern
- Implements a fragment to request runtime permissions
- Implements unit testing using Roboelectric
- Includes sample configuration for continuous integration using a [Docker image](https://github.com/omtinez/android-dev)
