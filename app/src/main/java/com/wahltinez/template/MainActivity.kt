package com.wahltinez.template

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.FrameLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.wahltinez.template.extensions.FLAGS_FULLSCREEN

/**
 * Main entry point into our app. This app follows the single-activity pattern, and all
 * functionality is implemented in the form of fragments.
 */
class MainActivity : AppCompatActivity() {
    private lateinit var container: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = findViewById(R.id.fragment_container)
    }

    override fun onResume() {
        super.onResume()

        // Before setting full screen flags, we must wait a bit to let UI settle; otherwise, we may
        // be trying to set app to immersive mode before it's ready and the flags do not stick
        container.postDelayed({
            container.systemUiVisibility = FLAGS_FULLSCREEN
        }, IMMERSIVE_TIMEOUT_MILLIS)
    }

    /**
     * When key event is triggered, relay it via local broadcast so fragments can handle it.
     * Then, in a fragment, listen to the key event by registering a broadcast receiver:
     *
     *     val broadcastManager = LocalBroadcastManager.getInstance(context)
     *     val broadcastFilter = IntentFilter().apply { addAction(ACTION_KEY_EVENT) }
     *     val broadcastReceiver = object : BroadcastReceiver() {
     *         override fun onReceive(context: Context, intent: Intent) { ... }
     *     }
     *     broadcastManager.registerReceiver(broadcastReceiver, broadcastFilter)
     *
     *  Don't forger to register the broadcast manager in onResume and unregister it in onPause.
     */
    fun onKeyEvent(keyCode: Int, event: KeyEvent): Boolean {
        val intent = Intent(ACTION_KEY_EVENT).apply {
            putExtra(EXTRA_KEY_EVENT, event)
            putExtra(EXTRA_KEY_CODE, keyCode)
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        return when (event.action) {
            KeyEvent.ACTION_UP -> super.onKeyUp(keyCode, event)
            KeyEvent.ACTION_DOWN -> super.onKeyDown(keyCode, event)
            else -> false
        }
    }

    // Handle up and down key events
    override fun onKeyUp(keyCode: Int, event: KeyEvent) = onKeyEvent(keyCode, event)
    override fun onKeyDown(keyCode: Int, event: KeyEvent) = onKeyEvent(keyCode, event)

    companion object {

        /** Time to wait before setting the immersive flag upon activity resume */
        private const val IMMERSIVE_TIMEOUT_MILLIS = 500L

        /** Intent action name for our key event dispatcher */
        const val ACTION_KEY_EVENT = BuildConfig.APPLICATION_ID + ".ACTION_KEY_EVENT"

        /** Intent extra key for key code in our key event dispatcher */
        const val EXTRA_KEY_EVENT = BuildConfig.APPLICATION_ID + ".EXTRA_KEY_EVENT"

        /** Intent extra key for key code in our key event dispatcher */
        const val EXTRA_KEY_CODE = BuildConfig.APPLICATION_ID + ".EXTRA_KEY_CODE"
    }
}