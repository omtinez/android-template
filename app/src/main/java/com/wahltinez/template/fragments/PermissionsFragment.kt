package com.wahltinez.template.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.wahltinez.template.R
import kotlin.random.Random


/**
 * The sole purpose of this fragment is to request permissions and, once granted, display the
 * camera fragment to the user.
 */
class PermissionsFragment : Fragment() {
    private val requestCode = Random.nextInt()

    override fun onStart() {
        super.onStart()
        if (!hasPermissions(requireContext())) {
            // Request our required permissions
            requestPermissions(PERMISSIONS_REQUIRED, requestCode)
        } else {
            // If permissions have already been granted, proceed to next fragment
            Navigation.findNavController(requireActivity(), R.id.fragment_container).navigate(
                    PermissionsFragmentDirections.actionPermissionsToHelloWorld())
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Take the user to the success fragment when permission is granted
                Toast.makeText(context, "Permission request granted", Toast.LENGTH_LONG).show()
                Navigation.findNavController(requireActivity(), R.id.fragment_container).navigate(
                        PermissionsFragmentDirections.actionPermissionsToHelloWorld())
            } else {
                // Kill the app if permissions were denied by the user
                Toast.makeText(context, "Permission request denied", Toast.LENGTH_LONG).show()
                requireActivity().finish()
            }
        }
    }

    companion object {

        /** List of permissions required by this app */
        val PERMISSIONS_REQUIRED = arrayOf(Manifest.permission.INTERNET)

        /** Convenience method used to check if all permissions required by this app are granted */
        fun hasPermissions(context: Context) = PERMISSIONS_REQUIRED.all {
            ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
        }
    }
}
