package com.wahltinez.template.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.wahltinez.template.R
import com.wahltinez.template.extensions.padSafeInsets

class HelloWorldFragment : Fragment() {

    /** AndroidX navigation arguments */
    private val args: HelloWorldFragmentArgs by navArgs()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_hello_world, container, false)
        view.findViewById<ConstraintLayout>(R.id.safe_inset_container).padSafeInsets()
        view.findViewById<TextView>(R.id.hello_world_text).text = "Hello ${args.name}"
        return view
    }

    override fun onResume() {
        super.onResume()

        // Make sure that all permissions are still present, since user could have removed them
        //  while the app was on paused state
        if (!PermissionsFragment.hasPermissions(requireContext())) {
            Navigation.findNavController(requireActivity(), R.id.fragment_container).navigate(
                    HelloWorldFragmentDirections.actionHelloWorldToPermissions())
        }
    }
}