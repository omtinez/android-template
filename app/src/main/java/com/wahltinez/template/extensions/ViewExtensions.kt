package com.wahltinez.template.extensions

import android.os.Build
import android.view.*
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import kotlin.math.max

/** Combination of all flags required to put activity into immersive mode */
const val FLAGS_FULLSCREEN =
        View.SYSTEM_UI_FLAG_LOW_PROFILE or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

/** Flags required to hide the navigation bar */
const val FLAGS_HIDE_NAV =
        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

/** Milliseconds used for UI animations */
const val ANIMATION_FAST_MILLIS = 50L
const val ANIMATION_SLOW_MILLIS = 100L

/**
 * Simulate a button click, including a small delay while it is being pressed to trigger the
 * appropriate animations.
 */
fun ImageButton.simulateClick(delay: Long = ANIMATION_FAST_MILLIS) {
    performClick()
    isPressed = true
    invalidate()
    postDelayed({
        invalidate()
        isPressed = false
    }, delay)
}

/** Pad this view with the insets provided by the device cutout (i.e. notch) */
fun View.padSafeInsets() {

    // Early exit: API level is below P, so there's no notch to support
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) return

    // Retrieve the layout parameters, necessary to query current margin
    val layout = layoutParams as ViewGroup.MarginLayoutParams

    /** Helper method that applies padding to ensure window safe insets are not drawn over */
    fun doPadding(insets: WindowInsets) = setPadding(
            max(0, max(paddingLeft, insets.systemWindowInsetLeft) - layout.leftMargin),
            max(0, max(paddingTop, insets.systemWindowInsetTop) - layout.topMargin),
            max(0, max(paddingRight, insets.systemWindowInsetRight) - layout.rightMargin),
            max(0, max(paddingBottom, insets.systemWindowInsetBottom) - layout.bottomMargin))

    // Apply padding using the display cutout designated "safe area"
    rootWindowInsets?.let { doPadding(it) }

    // Set a listener for window insets since view.rootWindowInsets may not be ready yet
    setOnApplyWindowInsetsListener { _, insets ->
        doPadding(insets)
        insets
    }
}

/** Same as [AlertDialog.show] but setting immersive mode in the dialog's window */
fun AlertDialog.showImmersive() {

    // Set the dialog to not focusable
    window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

    // Make sure that the dialog's window is in full screen
    window?.decorView?.systemUiVisibility = FLAGS_FULLSCREEN

    // Show the dialog while still in immersive mode
    show()

    // Set the dialog to focusable again
    window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
}
