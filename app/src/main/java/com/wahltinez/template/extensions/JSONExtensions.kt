package com.wahltinez.template.extensions

import org.json.JSONArray

/** Implements a list-like map for JSONArray objects */
fun <T>JSONArray.map(mapper: (Any) -> T): List<T> = (0 until length()).map { mapper(get(it)) }